from urllib import parse, request
import random, time

def main():

    # This data isn't very interesting. It's only one
    # reporter per location. TODO Change?
    my_dict = { "1232":"IBKS4AA00048089", "1233":"IBKS4AA00048090",
        "1234":"IBKS4AA00048089", "1235":"IBKS4AA00048090",
        "1236":"IBKS4AA00048089", "1237":"IBKS4AA00048090",
        "1238":"IBKS4AA00048089", "1239":"IBKS4AA00048090"
    }

    while (1):
        key, val = random.choice(list(my_dict.items()))

        data = parse.urlencode({ "reporter":key, "location":val })
        data = data.encode("ascii")

        req = request.Request("http://localhost:4567/update")
        req.add_header("Accept", "*/*")
        request.urlopen(req, data)

        print("Sent to {} as {}".format(val, key))
        time.sleep(0.5)

if __name__ == "__main__":
    main()