from urllib import parse, request
import time

OCCUPANCY_API = "http://localhost:4567/"

def main():

    # I am definitely going to write this to be a little more modular.
    # However, it will now be here just as a backup.

    print("Running fake client...")

    print("Obtaining ID...")
    id = send_read("a", { })

    print("Received {}".format(id))

    bc = "0007:0001"
    print("Sending report about beacon {}".format(bc))
    send_read("u", { "user":id, "beac":bc })

    time.sleep(2)

    print("Sending the same report again...")
    send_read("u", { "user":id, "beac":bc })

    time.sleep(2)

    print("Collecting information about fake entities...")
    re = send_read("o", { "rumppa":0, "0007:0001":0, "006000002":0 })
    print("Received {}".format(re))

    time.sleep(2)

    print("Collecting information about a real beacon...")
    re = send_read("o", { "0060:00002":0, "1401127":0 })
    print("Received {}".format(re))

    time.sleep(2)

    bc = "0001:0001"
    print("Adding a report to stop, too...")
    send_read("u", { "user":id, "beac":bc })

    print("Checking that both have value 1...")
    re = send_read("o", { "0060:00002":0, "1401127":0 })
    print("Received {}".format(re))

    time.sleep(2)

    print("Fetching new ID to increment stop value...")
    id = send_read("a", { })

    time.sleep(2)

    bc = "0001:0001"
    print("Sending the other report...")
    re = send_read("u", { "user":id, "beac":bc })

    print("Checking that stop has value 2...")
    re = send_read("o", { "0060:00002":0, "1401127":0 })
    print("Received {}".format(re))

def send_read(addr, data):

    reqt = request.Request(OCCUPANCY_API + addr)
    reqt.add_header("Accept", "*/*")

    data = parse.urlencode(data)
    data = data.encode("ascii")

    resp = request.urlopen(reqt, data)
    return resp.read().decode("utf-8")

def read(addr):

    return send_read(addr, None)

if __name__ == "__main__":
    main()