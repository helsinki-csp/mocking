# Mocking
All mocking stuff goes here. Maybe.

## report.py
* Requires Python 3
* No dependencies
* Current status: BROKEN

Continuously send POST data to backend /update/ route.

## demo_client.py
* Requires Python 3
* No dependencies
* Current status: WORKING

Test the main functionality of the backend.